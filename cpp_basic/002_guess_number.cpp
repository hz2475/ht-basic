#include<iostream>
#include<stdlib.h>
#include<time.h>

using namespace std;

int main(){
    // gen random number
    srand(time(NULL));
    int random_number = rand() % 1000;
    // guess
    int guessed_num = 0;
    while (guessed_num != random_number){
        if (guessed_num < random_number){
            cout << guessed_num <<" is too small, try again: ";
            cin >> guessed_num;
        }
        else if(guessed_num > random_number){
            cout << guessed_num <<" is too large, try again: ";
            cin >> guessed_num;
        }
    }
    cout << "you guessed it!" << endl;
}