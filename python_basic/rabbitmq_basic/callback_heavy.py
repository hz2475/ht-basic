import pika
import sys
import os
import json
from time import sleep
import os
import logging
import functools
import threading


def ack_message(channel, delivery_tag):
    """Note that `channel` must be the same pika channel instance via which
    the message being ACKed was retrieved (AMQP protocol constraint).
    """
    if channel.is_open:
        channel.basic_ack(delivery_tag)
    else:
        # Channel is already closed, so we can't ACK this message;
        # log and/or do something that makes sense for your app in this case.
        pass

def do_work(connection, channel, delivery_tag, body):

    # Sleeping to simulate 10 seconds of work
    message_dict = json.loads(body.decode())
    logging.info(" [x] 接单: {}号, {}道菜".format(message_dict['id'],message_dict['value']))
    sleep(message_dict['value']*2)
    logging.info(" [o] 上菜: {}号, {}道菜".format(message_dict['id'],message_dict['value']))
    cb = functools.partial(ack_message, channel, delivery_tag)
    connection.add_callback_threadsafe(cb)

def on_message(channel, method_frame, header_frame, body, args):
    (connection, threads) = args
    delivery_tag = method_frame.delivery_tag
    t = threading.Thread(target=do_work, args=(connection, channel, delivery_tag, body))
    t.start()
    threads.append(t)

# def callback(ch, method, properties, body):
#     try:
#         message_dict = json.loads(body.decode())
#         logging.info(" [x] 接单: {}号, {}道菜".format(message_dict['id'],message_dict['value']))
#         sleep(message_dict['value']*2)
#         logging.info(" [o] 上菜: {}号, {}道菜".format(message_dict['id'],message_dict['value']))
#         ch.basic_ack(delivery_tag = method.delivery_tag)
#     except:
#         logging.info(" [-] Wrong format")

def main():
    # setup logs
    log_path = "./runtime.log"
    open(log_path, 'a').close()
    logging.basicConfig(format='%(asctime)-22s %(levelname)-8s - %(message)s',
                        filename=log_path,
                        level=logging.INFO)
    # main stuff
    credentials = pika.PlainCredentials('guest', 'guest')
    # Note: sending a short heartbeat to prove that heartbeats are still
    # sent even though the worker simulates long-running work
    parameters =  pika.ConnectionParameters('localhost', credentials=credentials, heartbeat=5)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    channel.exchange_declare(exchange="test_exchange", exchange_type="direct", passive=False, durable=True, auto_delete=False)
    channel.queue_declare(queue="standard", auto_delete=True)
    channel.queue_bind(queue="standard", exchange="test_exchange", routing_key="standard_key")
    # Note: prefetch is set to 1 here as an example only and to keep the number of threads created
    # to a reasonable amount. In production you will want to test with different prefetch values
    # to find which one provides the best performance and usability for your solution
    channel.basic_qos(prefetch_count=1)

    threads = []
    on_message_callback = functools.partial(on_message, args=(connection, threads))
    channel.basic_consume(on_message_callback, 'standard')
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


