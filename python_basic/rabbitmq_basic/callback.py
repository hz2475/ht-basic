import pika
import sys
import os
import json
from time import sleep
import os
import logging

def callback(ch, method, properties, body):
    try:
        message_dict = json.loads(body.decode())
        logging.info(" [x] 接单: {}号, {}道菜".format(message_dict['id'],message_dict['value']))
        sleep(message_dict['value'])
        logging.info(" [o] 上菜: {}号, {}道菜".format(message_dict['id'],message_dict['value']))
    except:
        logging.info(" [-] Wrong format")

def main():
    # setup logs
    log_path = "./runtime.log"
    open(log_path, 'a').close()
    logging.basicConfig(format='%(asctime)-22s %(levelname)-8s - %(message)s',
                        filename=log_path,
                        level=logging.INFO)
    # main stuff
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.basic_consume(queue='hello',
                      auto_ack=True,
                      on_message_callback=callback)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


