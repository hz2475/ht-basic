import pika
import json
import random

class SimpleSender:
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        self.channel = self.connection.channel()
    def simple_publish(self, message, queue_name):
        self.channel.queue_declare(queue=queue_name)
        self.channel.basic_publish(exchange='',
                                   routing_key=queue_name,
                                   body=json.dumps(message))

if __name__ == "__main__":
    sender = SimpleSender()
    for i in range(100):
        message = {'id':i,
                   'value':int(random.random()*10)}
        sender.simple_publish(message,'hello')
        
