import pika
import sys
import os
import json
from time import sleep
import os
import logging
import functools
import logging
import threading

class SimpleConsumer:
    # main stuff
    def __init__(self):
        credentials = pika.PlainCredentials('guest', 'guest')
        # Note: sending a short heartbeat to prove that heartbeats are still
        # sent even though the worker simulates long-running work
        parameters =  pika.ConnectionParameters('localhost', credentials=credentials, heartbeat=5)
        self.connection = pika.BlockingConnection(parameters)
        self.queue_name = 'hello'
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange="test_exchange", exchange_type="direct", passive=False,
                                                                durable=True, auto_delete=False)
        self.channel.queue_declare(queue=self.queue_name)
        self.channel.queue_bind(queue=self.queue_name, exchange="test_exchange", routing_key="standard_key")

    @staticmethod
    def ack_message(channel, delivery_tag):
        """Note that `channel` must be the same pika channel instance via which 
        the message being ACKed was retrieved (AMQP protocol constraint).
        """
        if channel.is_open:
            channel.basic_ack(delivery_tag)
        else:
            # Channel is already closed, so we can't ACK this message;
            # log and/or do something that makes sense for your app in this case.
            pass

    def do_work(self, connection, channel, delivery_tag, body):

        # Sleeping to simulate 10 seconds of work
        try:
            ###### work
            message_dict = json.loads(body.decode())
            logging.info(" [x] 接单: {}号, {}道菜".format(message_dict['id'],message_dict['value']))
            sleep(message_dict['value']*2)
            logging.info(" [o] 上菜: {}号, {}道菜".format(message_dict['id'],message_dict['value']))
            ######
        except KeyboardInterrupt:
            print('Interrupted')
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
        except:
            logging.info(" [-] Wrong format")
        
        cb = functools.partial(self.ack_message, self.channel, delivery_tag)
        self.connection.add_callback_threadsafe(cb)

    def on_message(self, channel, method_frame, header_frame, body, args):
        (connection, threads) = args
        delivery_tag = method_frame.delivery_tag
        t = threading.Thread(target=self.do_work, args=(connection, channel, delivery_tag, body))
        t.start()
        threads.append(t)


    def consume_forever(self):
        # Note: prefetch is set to 1 here as an example only and to keep the number of threads created
        # to a reasonable amount. In production you will want to test with different prefetch values
        # to find which one provides the best performance and usability for your solution
        self.channel.basic_qos(prefetch_count=1)

        threads = []
        on_message_callback = functools.partial(self.on_message, args=(self.connection, threads))
        # print(self.queue_name)
        self.channel.basic_consume(queue = self.queue_name, on_message_callback = on_message_callback, )
        print(' [*] Waiting for messages. To exit press CTRL+C')
        self.channel.start_consuming()

def main():
    # setup logs
    log_path = "./runtime.log"
    open(log_path, 'a').close()
    logging.basicConfig(format='%(asctime)-22s %(levelname)-8s - %(message)s',
                        filename=log_path,
                        level=logging.INFO)
    consumer = SimpleConsumer()
    consumer.consume_forever()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


